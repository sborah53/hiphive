.. _fcs_sensing:
.. highlight:: python
.. index::
   single: Force constant sensing

.. |br| raw:: html

  <br/>


Representing FCs from external sources as a FCP
===============================================

There exist several codes that allow one to compute force constants and
:program:`hiphive` provides :ref:`functionality to import some of these formats
<force_constants_io>`, including :program:`phonopy`, :program:`phono3py`, and
:program:`thirdorder.py` (ShengBTE). It can be convenient to represent these
force constants as :class:`ForceConstantPotential
<hiphive.ForceConstantPotential>` objects, for example in order :ref:`to
enforce rotational sum rules <rotational_sum_rules>`.

Firstly these force constants :ref:`have to be imported <force_constants_io>`
as a :class:`ForceConstants <hiphive.force_constants.ForceConstants>` object.
Secondly, one has to construct the :class:`ClusterSpace <hiphive.ClusterSpace>`
on which the force constants will be projected. Then one can obtain the
parameters as follows::

    from hiphive.utilities import extract_parameters
    parameters = extract_parameters(fcs, cs)

Now a :class:`ForceConstantPotential <hiphive.ForceConstantPotential>` can be
created by combining the :class:`ClusterSpace <hiphive.ClusterSpace>` and the
parameters in the usual manner::

    fcp = ForceConstantPotential(cs, parameters)


.. container:: toggle

    .. container:: header

       A minimal example can be found in |br|
       ``tests/integration/fcs_sensing.py``

    .. literalinclude:: ../../../tests/integration/fcs_sensing.py
