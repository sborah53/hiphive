# Base image
FROM continuumio/miniconda

# Base packages
RUN \
  apt-get update -qy && \
  apt-get upgrade -qy && \
  apt-get install -qy \
    graphviz \
    python3-pip \
    zip

RUN \
  pip3 install --upgrade \
    pip \
  && \
  pip3 install --upgrade \
    coverage \
    flake8 \
    xdoctest

# Packages needed for hiphive
RUN \
  pip3 install \
    h5py \
    numba \
    numpy \
    sympy \
  && \
  pip3 install \
    ase \
    scikit-learn \
    scipy \
    spglib

# Packages for building documentation
RUN \
  pip3 install --upgrade \
    sphinx_autodoc_typehints \
    sphinx-rtd-theme \
    sphinx_sitemap \
    sphinxcontrib-bibtex \
    cloud_sptheme

# Packages for running examples
RUN \
  pip3 install \
    pandas \
    phonopy \
  && \
  conda install -c atztogo phono3py

CMD /bin/bash
